import { render as reactDomRender } from "react-dom";
import { BrowserRouter } from "react-router-dom";

import { RootLayout } from "features";
import { MainRouteSwitch } from "routes/MainRouteSwitch";
import reportWebVitals from "./reportWebVitals";

reactDomRender(
  <BrowserRouter>
    <RootLayout>
      <MainRouteSwitch />
    </RootLayout>
  </BrowserRouter>,
  document.getElementById("root")
);

reportWebVitals(); // See https://bit.ly/CRA-vitals for details
