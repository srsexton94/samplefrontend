export enum FormNames {
  ADDRESS = "mailAddress",
  UNIT = "unit",
  CITY = "city",
  STATE = "state",
  ZIP = "zipCode",
  PHONE = "phoneNumber",
}

export interface FormValues {
  [FormNames.ADDRESS]: string;
  [FormNames.UNIT]?: string;
  [FormNames.CITY]: string;
  [FormNames.STATE]: string;
  [FormNames.ZIP]: string;
  [FormNames.PHONE]: string;
}

export const FormLabels: { [key in FormNames]: string } = {
  [FormNames.ADDRESS]: "Mailing Address",
  [FormNames.UNIT]: "Apt, Suite, Unit, etc.",
  [FormNames.CITY]: "City",
  [FormNames.STATE]: "State",
  [FormNames.ZIP]: "ZIP Code",
  [FormNames.PHONE]: "Mobile Number",
};
