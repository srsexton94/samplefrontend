import styled from "styled-components/macro";

import { vars } from "styles/vars.css";

const FieldsetStyles = styled.fieldset`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  width: 41.5rem;
  max-width: 95vw;
  border: 0;
  margin: 0;
`;

const LegendStyles = styled.legend`
  position: absolute;
  clip: rect(0 0 0 0);
  clip-path: inset(50%);
  height: 0.0625rem;
  width: 0.0625rem;
  overflow: hidden;
  white-space: nowrap;
`;

const SubmitStyles = styled.input`
  display: block;
  margin: 2rem auto;
  height: 3.5rem;
  width: 40rem;
  max-width: 90vw;
  background-color: ${vars.color.button};
  border: 0;
  border-radius: 0.5rem;
  color: white;
  cursor: pointer;
  font-weight: 700;
  text-shadow: 0 0.125rem 0.5rem ${vars.color.medGrey};
  text-transform: uppercase;
  &:hover,
  &:active {
    background-color: ${vars.color.buttonHighlight};
  }
  &:active {
    box-shadow: 0 0.5rem 0.75rem 0 ${vars.color.medGrey};
    transform: translateY(4px);
  }
`;

export { FieldsetStyles, LegendStyles, SubmitStyles };
