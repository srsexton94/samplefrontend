import { render, screen } from "@testing-library/react";
import { Form } from "./Form";

test("renders the Form", () => {
  render(<Form />);
  const fieldsets: HTMLFieldSetElement[] = screen.getAllByRole("group");
  const inputs: HTMLInputElement[] = screen.getAllByRole("textbox");
  const dropdown: HTMLInputElement = screen.getByRole("combobox");

  expect(fieldsets.length).toBe(3);
  expect(inputs.length).toBe(5);
  expect(dropdown).toBeInTheDocument();
});
