import { FC } from "react";
import { useForm } from "react-hook-form";

import { FormNames, FormValues } from "models";
import { FormInput } from "patterns";
import { FieldsetStyles, LegendStyles, SubmitStyles } from "./form.css";

export const Form: FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<FormValues>();

  const { ADDRESS, CITY, PHONE, STATE, UNIT, ZIP } = FormNames;
  const formControls = { register, errors };

  return (
    <form onSubmit={handleSubmit((d) => console.log(JSON.stringify(d)))}>
      <FieldsetStyles>
        <LegendStyles>Street Address</LegendStyles>
        <FormInput formName={ADDRESS} formControls={formControls} />
        <FormInput formName={UNIT} formControls={formControls} />
      </FieldsetStyles>
      <FieldsetStyles>
        <LegendStyles>City and State</LegendStyles>
        <FormInput formName={CITY} formControls={formControls} />
        <FormInput formName={STATE} formControls={formControls} />
      </FieldsetStyles>
      <FieldsetStyles>
        <LegendStyles>ZIP and Phone</LegendStyles>
        <FormInput formName={ZIP} formControls={formControls} />
        <FormInput formName={PHONE} formControls={formControls} />
      </FieldsetStyles>
      <SubmitStyles type="submit" value="Save and Continue" />
    </form>
  );
};
