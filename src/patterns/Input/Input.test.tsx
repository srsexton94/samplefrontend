import { render, screen } from "@testing-library/react";
import { UseFormRegister } from "react-hook-form";

import { FormNames, FormValues } from "models";
import { FormInput } from "./Input";

const testFormControls = {
  errors: {} as Record<string, unknown>,
  register: new Function() as UseFormRegister<FormValues>,
};

test("renders an input", () => {
  render(<FormInput formName={FormNames.ADDRESS} formControls={testFormControls} />);

  expect(screen.getByRole("textbox")).toBeInTheDocument();
});
