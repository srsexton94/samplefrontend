import { useEffect, useState } from "react";

import { states, FormNames, FormLabels, FormValues } from "models";
import { InputError, InputContainer } from "./Input.css";
import { UseFormRegister } from "react-hook-form";

const fullLineInputs: FormNames[] = [FormNames.ADDRESS, FormNames.UNIT];
interface InputProps {
  formName: FormNames;
  formControls: {
    errors: Record<string, unknown>;
    register: UseFormRegister<FormValues>;
  };
}
// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export function FormInput({ formControls: { errors, register }, formName }: InputProps) {
  const [labelState, setLabelState] = useState("");

  const [inputId, errorId, listId] = ["Input", "Error", "List"].map((id) => formName + id);
  const inputWidth: string = fullLineInputs.includes(formName) ? "full" : "half";
  const isRequired: boolean = formName !== FormNames.UNIT;
  const isDropdown: boolean = formName === FormNames.STATE;
  const altLabel: string | boolean = !isRequired && FormLabels.unit.replace(/apt/i, "Apartment");

  useEffect(() => {
    const inputEl = document.getElementById(inputId) as HTMLInputElement;
    inputEl?.addEventListener("blur", () => {
      setLabelState(inputEl.value ? "retracted" : "");
    });
  });

  return (
    <InputContainer className={`${inputWidth}${errors[formName] ? " error" : ""}`}>
      <label className={labelState} aria-label={altLabel || undefined} htmlFor={inputId}>
        {FormLabels[formName]}
      </label>
      <input
        id={inputId}
        list={isDropdown ? listId : undefined}
        type="text"
        aria-describedby={errors[formName] ? errorId : undefined}
        aria-required={isRequired}
        onFocus={() => setLabelState("retracted")}
        {...register(formName, { required: isRequired })}
      />
      {isDropdown && (
        <datalist id={listId}>
          {states.map(({ abbreviation, name }) => (
            <option key={abbreviation} value={name}>
              {name}
            </option>
          ))}
        </datalist>
      )}
      {errors[formName] && (
        <InputError id={errorId}>{`Please provide a ${FormLabels[formName]}`}</InputError>
      )}
    </InputContainer>
  );
}
