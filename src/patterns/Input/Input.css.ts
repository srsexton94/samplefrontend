import styled from "styled-components/macro";

import { vars } from "styles/vars.css";

const InputContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
  margin: 0;
  max-width: 90vw;

  &.full {
    width: 40rem;
  }
  &.half {
    width: 19rem;
  }
  &.error {
    label {
      color: ${vars.color.errorText};
    }
    input {
      background-color: ${vars.color.errorShade};
      border: 0.125rem solid ${vars.color.errorText};
    }
  }

  &:hover label,
  label.retracted {
    top: 1rem;
    font-size: 75%;
  }

  label,
  input {
    position: relative;
    display: inline-block;
  }

  label {
    color: ${vars.color.medGrey};
    top: 2.5rem;
    left: 1rem;
    transition: all 0.5s ease-in-out;
    z-index: 1;
  }

  input {
    height: 4rem;
    max-width: 90vw;
    padding: 1rem 1.5rem;
    border: 0;
    border-radius: 0.5rem;
    box-shadow: 0 0 0.5rem 0.125rem ${vars.color.inputShadow};
  }
`;

const InputError = styled.p`
  display: inline-block;
  color: ${vars.color.errorText};
  margin: 0.5rem;
`;

export { InputError, InputContainer };
