export const vars = {
  fontFamily: {
    body: `Inter, Helvetica, Arial, sans-serif`,
  },
  color: {
    background: "#f6f6f6",
    errorShade: "#ffe0e0",
    errorText: "#ff4138",
    button: "#02adee",
    buttonHighlight: "#0db9fd",
    medGrey: "#666666",
    inputShadow: "#c9c9c9",
  },
  breakpoints: {
    value: {
      mobile: 0,
      tablet: "48em",
      desktop: "64em",
      desktopLg: "90em",
    },
    writeMediaQuery: (breakpoint: string): string => `@media screen and (min-width: ${breakpoint})`,
  },
  modes: {
    lightMode: "",
    darkMode: "@media (prefers-color-scheme: dark)",
  },
};
