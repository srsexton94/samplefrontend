import { render, screen } from "@testing-library/react";
import { Home } from "./Home";

test("renders the Home Page", () => {
  render(<Home />);

  expect(screen.getByRole("main")).toBeInTheDocument();
  expect(screen.getByRole("heading")).toHaveTextContent(/Please provide your address/i);
});
