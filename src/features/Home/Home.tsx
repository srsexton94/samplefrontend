import { FC } from "react";
import { Form } from "patterns";
import { HomeStyles } from "./home.css";

export const Home: FC = () => {
  return (
    <HomeStyles>
      <h1>Please provide your address</h1>
      <Form />
    </HomeStyles>
  );
};
