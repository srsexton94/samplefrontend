import styled from "styled-components/macro";

export const HomeStyles = styled.main`
  display: flex;
  align-items: center;
  flex-direction: column;
  min-height: 45rem;
  width: 62rem;
  max-width: 80vw;
`;
