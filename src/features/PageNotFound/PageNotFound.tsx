import { FC } from "react";

import { PageNotFoundStyles } from "./pageNotFound.css";

export const PageNotFound: FC = () => {
  return (
    <PageNotFoundStyles>
      <h1>Page Not Found</h1>
    </PageNotFoundStyles>
  );
};
