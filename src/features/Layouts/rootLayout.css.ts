import { ReactNode } from "react";
import styled from "styled-components/macro";
import { vars } from "styles/vars.css";

export interface IRootLayoutStyles {
  children: ReactNode;
}

export const RootLayoutStyles = styled.div<IRootLayoutStyles>`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: ${vars.color.background};
  color: black;
  width: 100vw;
  min-height: 100vh;
  padding: 2rem;
`;
