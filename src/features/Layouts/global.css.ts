import { createGlobalStyle } from "styled-components/macro";
import { normalize } from "styled-normalize";
import { vars } from "styles/vars.css";

export const GlobalStyles = createGlobalStyle`
    body, body * {
        box-sizing: border-box;
        font-family: ${vars.fontFamily.body};
    }
    body {
        background-color: #fff;
        color: #000;
    }
    ${normalize}
`;
