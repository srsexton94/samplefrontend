# Philanthropi Phorm

Hey Philanthropi! Here's my take on your take-home test. You provided a repo built with [`create-react-app`](https://github.com/facebook/create-react-app) and [`react-hook-form`](https://react-hook-form.com/), on Node v14.18.1 and tasked me with the following:

1. Create a form that submits data & validates the inputs (`console.log` "submission" OK for MVP)
2. Match the provided design using [`styled-components`](https://styled-components.com/)

- ![example form](form-design.png "example form")

3. Optionally setup a fake API call that submits the address data.

- Use an endpoint like `https://jsonplaceholder.typicode.com/posts` to `POST` the data. You should receive the same data back that you send.
- We included Axios and React-Query as libraries to use for API handling.

4. Optionally add tests for new components and features

## Scripts

- `yarn start` || `npm run start` (runs dev mode at [http://localhost:3000](http://localhost:3000))
- `yarn test` || `npm run test` (runs tests in interactive watch mode: see [running tests](https://facebook.github.io/create-react-app/docs/running-tests))
- `yarn build` || `npm run build` (bundles & optimizes for production in `build/` folder)

## Results

![empty result](result-empty.png "empty result")
![error result](result-error.png "error result")
![filled result](result-filled.png "filled result")

## TODOs

Currently the app sits as an accessible web form that mostly matches the design specs, validates the inputs, is (barely) tested, and "submits" via console.log.

With more time I will/would...

- Replace the console.log with a fake API call
- Add to tests to account for conditionality (possibly through a testing framework - Jest?)
- investigate how to use onBlur within an input using react-hook-form's register method (duplicates attribute) to avoid using a document listener
- improve layout styling so that the inputs stay in place between valid/invalid states
- investigate how to use media queries in styled-components and improve mobile styling at breakpoints 340-670px
- Refactor Form component to sit at own route (to utilize "Back" button in design specs)
